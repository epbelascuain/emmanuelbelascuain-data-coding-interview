import os
import boto3
import json
import pandas as pd
from sqlalchemy import create_engine

#Secret stored on AWS Secrets manager -> {"host":"localhost","user":"postgres","password":"Password1234**"}

class PostgresPandas:            
    def __init__(self, secret: str, database: str):
        """
        Initializes a Postgres object and establishes a connection to the PostgreSQL database.

        Args:
            secret (str): The name or ARN of the AWS Secrets Manager secret containing the database credentials.
            database (str): The name of the PostgreSQL database.
        """
        self.response = json.loads(boto3.client('secretsmanager').get_secret_value(SecretId=secret)["SecretString"])
        self.host = self.response["host"]
        self.user = self.response["user"]
        self.password = self.response["password"]
        self.engine = create_engine(f'postgresql://{self.user}:{self.password}@{self.host}:5432/{database}')


    def getSourcePath(self) -> str:
        """
        Returns the absolute path of the dataset directory.

        Returns:
            str: The absolute path of the dataset directory.
        """
        current_path = os.path.dirname(os.path.abspath(__file__))
        dataset_path = 'challenge1\dataset'
        return f'{current_path}\{dataset_path}'
    

    def loadAll(self, max_retries:int = 3):
        """
        Loads all CSV files in the dataset directory into their corresponding database tables.

        Args:
            max_retries (int, optional): The maximum number of retries for failed insertions. Defaults to 3.
        """
        retry_list = []
        directory = self.getSourcePath()
        for filename in os.listdir(directory):
            full_path = os.path.join(directory, filename)
            if filename.endswith('.csv'):
                table_name = filename[:-4].split("_")[1]
                inserted = self.insertRecords(full_path, table_name, 'replace')
                if not inserted:
                    retry_list.append(table_name)

        while len(retry_list) > 0 and max_retries > 0:
            max_retries -= 1
            for table in retry_list:
                inserted = self.loadTable(table)
                if inserted:
                    retry_list.remove(table)


    def loadTable(self, table_name:str) -> bool:
        """
        Loads a specific table from a CSV file into the corresponding database table.

        Args:
            table_name (str): The name of the table to load.

        Returns:
            bool: True if the insertion is successful, False otherwise.
        """
        table_name = table_name.lower()
        directory = self.getSourcePath()
        full_path = os.path.join(directory, f'nyc_{table_name}.csv')
        inserted = self.insertRecords(full_path, table_name, 'replace')
        return inserted


    def insertRecords(self, full_path:str, table_name:str, method:str='append') -> bool:
        """
        Inserts records from a CSV file into a database table.

        Args:
            full_path (str): The full path of the CSV file.
            table_name (str): The name of the database table.
            method (str, optional): The insertion method. Defaults to 'append'.

        Returns:
            bool: True if the insertion is successful, False otherwise.
        """
        inserted = False
        try:
            print(f"LOADING TABLE {table_name}")
            df = pd.read_csv(full_path)
            df.to_sql(table_name, self.engine, if_exists=method, index=False)
            inserted = True
            print(f"{table_name} LOADED SUCCESSFULLY.")
        except Exception as e:
            print(f"{table_name} LOAD FAILED.")
            print(e)
        return inserted


    def getRecords(self, table:str):
        """
        Retrieves records from a database table.

        Args:
            table (str): The name of the table.

        Returns:
            pandas.DataFrame: The retrieved records as a DataFrame.
        """
        query = f"SELECT * FROM {table}"
        return pd.read_sql(query, self.engine)


    def join(self, table1, table2, join_col1, join_col2, join_type="inner"):
        """
        Performs a join operation between two database tables and returns the result.

        Args:
            table1 (str): The name of the first table.
            table2 (str): The name of the second table.
            join_col1 (str or list): The join column(s) in the first table.
            join_col2 (str or list): The join column(s) in the second table.
            join_type (str, optional): The type of join. Defaults to 'inner'.

        Returns:
            pandas.DataFrame: The result of the join operation.
        """
        df1 = self.getRecords(table1)
        df2 = self.getRecords(table2)

        if isinstance(join_col1, str):
            join_col1 = [join_col1]
        if isinstance(join_col2, str):
            join_col2 = [join_col2]

        for column_1, column_2 in zip(join_col1, join_col2):
            df1 = df1.rename(columns={column_1: column_2})
    
        return pd.merge(df1, df2, on=join_col2, how=join_type)
    


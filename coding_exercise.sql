--NOTE - Luis: CTE do not improve performance, it was just part of the coding exercise.
with f as (
	 select 
		lpad(sched_arr_time::VARCHAR, '4', '0') hours_minutes
	 from FLIGHTS   
),
s as (
	select 
		substring(f.hours_minutes, 1,2) hours,
		substring(f.hours_minutes, 3,4) minutes
	from f
)
select hours || ':' || minutes hours_minutes from s
;
